"""
A python application that sleeps
"""

import os
import socket
import sys
import time

#node = None #  in version 0.1
node = os.getenv("MY_NODE_NAME") # in version 0.2

print(f"Hi, I'm sleeper instance {socket.gethostname()} from running on {node}", file=sys.stdout, flush=True)
time.sleep(24800)
