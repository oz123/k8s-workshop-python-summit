"""
A python application that handles errors
"""

from logging import StreamHandler
from requestlogger import WSGILogger, ApacheFormatter

import o3
from bottle import Bottle, run, request

c = o3.Config()

version = c.version

app = Bottle()

@app.get("/")
def index():
    return {'hello': 'env-reader',
            'config': {'version': version,
                       'max_conn': c.max_conn,
                       'username': c.username,
                       'password': c.password}}

@app.get("/health/")
def health():
    return {'health': 'ok'}

app_w_logging = WSGILogger(app,
                           [StreamHandler(),],
                           ApacheFormatter(),
                           ip_header='X-Forwarded-For')

run(app=app_w_logging, server='bjoern', host='0.0.0.0')
