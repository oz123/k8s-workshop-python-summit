"""
A python application that sleeps
"""

import os
import socket
import sys
import time

from logging import StreamHandler
from requestlogger import WSGILogger, ApacheFormatter

from bottle import Bottle, run

node = os.getenv("MY_NODE_NAME")
# induce a small timeout, so that deployments are slow
# this is 'simulating' a database migration
time.sleep(5)

app = Bottle()

@app.get("/")
def index():
    return {'hello from': {'pod': socket.gethostname(),
                            'node': node,
                           'version': os.getenv('APP_VERSION')}}

@app.get("/health/")
def health():
    return {'health': 'ok'}

app_w_logging = WSGILogger(app, [StreamHandler(),], ApacheFormatter())

run(app=app_w_logging, server='bjoern', host='0.0.0.0')
