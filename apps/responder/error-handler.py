"""
A python application that handles errors
"""

from logging import StreamHandler
from requestlogger import WSGILogger, ApacheFormatter

from bottle import Bottle, run, request

app = Bottle()

@app.get("/")
def index():
    return {'status': 'error',
            'code': request.headers.get("X-Code", '418')}

@app.get("/health/")
def health():
    return {'health': 'ok'}

app_w_logging = WSGILogger(app,
                           [StreamHandler(),],
                           ApacheFormatter(),
                           ip_header='X-Forwarded-For')

run(app=app_w_logging, server='bjoern', host='0.0.0.0')
